# Jiannei 的网络日志

[![NPM version](https://img.shields.io/npm/v/brick.js.svg?style=flat)](https://www.npmjs.org/package/brick.js)
[![pipeline status](https://gitlab.com/JianNei/posts/badges/master/pipeline.svg)](https://gitlab.com/JianNei/posts/commits/master)
[![Build Status](https://www.travis-ci.org/JianNei/posts.svg?branch=pages)](https://www.travis-ci.org/JianNei/posts)

## 使用 vuepress 

```
yarn add -D vuepress@next
yarn add -D @vuepress/plugin-back-to-top@next
yarn add -D @vuepress/plugin-medium-zoom@next
```

## 使用 commitizen

```
npm install -D commitizen cz-conventional-changelog
# or yarn -D commitizen cz-conventional-changelog
# or cnpm install -D commitizen cz-conventional-changelog
```

## 日常

### 配置多个远程仓库

* 推送 docs 文档到远程 master
* 创建空的 pages 分支 `git checkout --orphan pages` （可选，如果想要把文档单独存放一个分支可以操作此项）
* 需要配置 vuepress config.js 中的 dest 为 public （gitlabci硬性要求），表示生成的静态文件放在项目根目录下的 public 目录
* 需要修改 .travis.yml 文件中的 local_dir 为 public (修改前为 docs/.vuepress/dist)
* 配置不同备注的 ssh-keys （也可以使用不同生成规则）

### github pages 与 gitlab pages 区别

* gitlab pages 可以选择提交源文档到 pages 分支，生成静态资源为根目录的 public
* github pages 默认是 gh-pages 分支，需要配合 travis-ci
* github pages 提交源文档到 master 分支，生成静态站点到 gh-pages 分支，可以拉取分支代码进行下载
* gitlab ci 生成的静态站点文件打包成 artifacts，可以到项目的 ci 进行下载

### gitlab

* `npm run docs:deploy`
* `setting -> pages`
* 注意 config.js 中配置 dest 为 public
* gitlab runner 配置（默认为 share runner ?） 

### github

* 配置 GITHUB_TOKEN
* 启用 travis-ci

### icon

* gitlab ci 图标：`Setting -> CI/CD -> Pipeline status`
* travis ci 图标：travis 项目名称旁边

## TODO

* bash 命令配合 husky
* ~~提交源文档到 master 生成静态站点到 pages 分支（监听 master 分支变化，生成静态站点到 pages 分支）~~
* 同步推送到码云等其他托管平台
