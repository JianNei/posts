#!/usr/bin/env sh

# dos2unix deploy.sh

set -e

# 配置

git config --local user.name "Jiannei"
git config --local user.email "longjian.huang@aliyun.com"

# 提交代码触发 Ci 部署到静态站点
git add -A

npm run commit

# 拉取代码
# git pull --rebase

# 推送
git push -u origin master