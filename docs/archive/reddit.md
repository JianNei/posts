# 产品分析：美国贴吧 Reddit

## 一．产品概述

### 1. 体验环境

体验版本：v3.6.0.212379，Google Play

体验设备：华为 Nova

体验时间：2018.2~2018.7

### 2. 产品简介

![reddit-analysis-2019-4-28-10-25-15.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-25-15.png)

Reddit 在的 slogan 是『The front page of the internet』。Reddit 成立于 2005 年，限于特殊的英文网络环境，其在国内用户不多，但是在欧美地区 Reddit 是一个有大量国际用户和巨大影响力的提供新闻、社交、用户分享等功能的综合性平台。

### 3. 产品定位
Reddit 是一个社交、娱乐和新闻平台，它的核心内容是多达上千的 subreddit。官方鼓励用户在遵守各个 subreddit 规定的前提下下分享和创作，同时通过精准的算法推荐，用户可以实时查看新的帖子。

### 4. 用户需求分析

#### 4.1 用户是谁

![reddit-analysis-2019-4-28-10-26-34.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-26-34.png)

根据 Pew Research Centre 于 2016 年的一篇报告，美国国内的 Reddit 用户大多是年轻、有较高学历且熟悉互联网的用户，同时这些人大多属于中高收入人群，以主流白人为主，政治观点偏左。

![reddit-analysis-2019-4-28-10-26-56.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-26-56.png)

事实上 Reddit 是一个非常国际化的平台，美国用户大概占到 41%，考虑到英文网络环境，其他用户大多来自英语国家和欧洲国家。

#### 4.2 用户特征

![reddit-analysis-2019-4-28-10-27-55.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-27-55.png)

排名前十的 subreddit 覆盖的领域主要在新闻、游戏、段子、体育、图片、视频、问答，这一点同其他社交类平台，尤其是年轻男性用户多的平台（例如百度贴吧）非常相似。

Reddit 用户通常有以下特征：

* 喜欢简洁明了的界面，快速、高效的看帖、发帖系统；
* 希望迅速找到自己喜欢的 subreddit，既可以通过搜索也可以由 Reddit 算法推荐；
* 乐于分享文字、图片，熟练使用社交功能诸如交友、聊天室。

#### 4.3 用户需求

根据经典的马斯洛需求层次可知，越是底层越是刚需，这一类产品通常是工具类产品，用户粘性不足，但是这类产品的生命力长久。而贴近高层需求，例如社交、尊重需求的产品，则很容易带给用户新鲜感，但是如果无法保证带有工具化的需求或运营不到位，很容易造成昙花一现。

Reddit 作为一款优秀的社交类产品，得益于把控住人性，满足了大部分用户的需求，其中包括：

* 核心社交需求，找到志趣相同的社区，交友；
* 资讯需求即国际、国内新闻，能迅速了解从各个渠道汇总的消息；
* 娱乐需求如段子、笑话、动图、短视频；
* 自我实现的需求，包括晒照、晒作品、秀恩爱，分享、产生内容；
* 安全性、匿名性需求。

#### 4.4 解决方案

技术手段上 Reddit 将自家项目部署在 AWS 上，使用数据库 Apache Cassandra、中间件 RabbitMQ、HAProxy 和缓存 memcached 来应对高并发和负载均衡。产品设计上主要是以下几个措施来保证用户需求和满意度：

* 简洁高效的 UI 设计，图标意义明了，各个模块分明；
* 提供全面多样的功能，找到功能的操作步骤少，新帖、热帖推荐算法精准；
* 严格的官方运营管理和 subreddit 版主自主管理，保证匿名性和主题相关性。

#### 4.5 用户使用场景

场景一：

美国东海岸大学生 Garrett，下午下课后拿出手机上 Reddit 闲逛，先到 r/gaming 板块看看有没有新的游戏新闻，再去 r/PrequelMemes 看星战前传系列的 meme，最后去 r/worldnews 阅读最新国际新闻，浏览帖子下的评论时对某个支持特朗普的评论不满，于是点 Reply 反驳。

场景二：

Simons 是一个 45 岁的英国大叔，周日下午整理自家杂物时发现了爷爷在二战时服役于 RAF 的军装照，于是去 r/OldSchoolCool 发 post，标题“Picture of my grandfather, WWII”, 几小时后获得 6k 赞和 200 多条评论，Simons 很热心的给跟帖的用户回答他爷爷的故事。

场景三：

r/Rainbow6 板块的玩家们发帖抱怨游戏内外挂众多，驻扎在板块内的育碧游戏官方运营立刻回应说会处理外挂。两天后，官方版主发了置顶帖“Anti-Cheat Next Steps”，声明育碧官方将采取技术和法律手段严惩外挂，帖子在几小时内获得玩家大量点赞。

### 5. 市场现状

#### 5.1. 行业分析

![reddit-analysis-2019-4-28-10-34-21.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-34-21.png)

目前，全球活跃的社交类用户多达 31 亿，其中移动端的用户数接近 30 亿，市场巨大。社交类应用满足社交需求、尊重需求以及部分自我实现的需求。鉴于大部分国家年轻人口教育水平和经济水平的提升，未来社交类应用极具潜力。社交类应用的盈利模式主要是广告、增值服务、会员、电商，而新闻类应用的盈利模式则是广告、会员阅读服务（例如华尔街日报）。

Reddit 是一个综合了社交、娱乐和新闻的平台，目前其应用内主要盈利方式是呈现形似热帖的赞助广告。移动端应用的屏幕空间有限，所以过多的广告会严重影响用户体验。作为平台来说，Reddit 吸引了巨大的流量，也收集到大量的用户数据，完全可以依靠大数据精准分析用户品味，和电商合作。

#### 5.2. 市场数据

![reddit-analysis-2019-4-28-10-35-1.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-35-1.png)

由图可知，国外市场 Facebook 占据着社交类应用中巨大的市场，而国内则是微信。由于 Reddit 依赖于英文网络环境，主打文字类内容，门槛稍微高一些，所以流行度不及纯社交、图片分享类应用。

![reddit-analysis-2019-4-28-10-35-21.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-35-21.png)

虽然 Reddit 内各大 subreddit 呈现出一片火热的景象，但是与社交巨头尤其是社交、图片分享类的 snapchat、instagram 和 pinterest 比，Reddit 仍然是个偏小众的应用。所幸 Reddit 的同质化竞争对手较少，与其类似的较为有名的社区平台是 4chan，但是 4chan 主要和 ACG 文化相关，用户重叠度较小。

国内的类似平台是百度贴吧，由于语言文化区别，两者几乎不构成竞争关系。截止 2018 年 6 月，Reddit 用户基数多达 3 亿 3000 万，其总估值在 2017 年时已接近 18 亿美元，而其员工数只有两百多人。

## 二．产品分析

### 1. 产品结构

![reddit-analysis-2019-4-28-10-36-22.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-36-22.png)

从个人体验而言，Reddit 的产品经理对于用户人性的把握非常精准。

底部最显眼的五个标签按照大多数用户阅读习惯从左到右依次是主页、关注的 subreddit、发帖、消息和邮件。

很多用户使用 Reddit 时以看帖、发评论为主，因此主页内容模块的划分非常清晰；在关注的 subreddit 里，特地区分出特别收藏的和全部收藏的，并且在以阅读内容为主的两大模块里都置顶显示了搜索栏，最大程度地减少用户检索信息的成本。

用户的个人信息部分内容较多，但是 Reddit 并没有杂乱地呈现出来，而是仅仅在左上角提供一个用户头像，用户相关的功能入口全在左侧栏。绝大部分阅读操作（无论是帖子还是邮件）都可以在两次点击内完成，而发帖步骤则只需要三步。

从目前的布局可以发现，Reddit 的核心还是 subreddit 和发帖、回帖，聊天功能居于次席，而在邮箱功能中最重要的、默认显示的也是帖子相关的通知。

### 2. 用户流程

![reddit-analysis-2019-4-28-10-37-37.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-37-37.png)

由用户流程可见，Reddit 的大部分精细化操作都在于找板块、看帖和评论，这也是活跃用户花费时间最多的地方。

值得一提的是，Reddit 帖子内容里从左到右第一个操作是点赞（Upvote）或踩（Downvote），热帖推荐的算法很大程度上是基于赞和踩而非单纯是评论数，这一设计也被 Facebook 部分地借鉴，旨在提供更有价值、更有建设性的内容，而非依靠情绪化的评论来分析用户产生的内容。

### 3. 功能体验

#### 3.1 简洁优雅的看帖、回帖，不喧宾夺主的功能细致化

**看帖：**

Card 模式

![reddit-analysis-2019-4-28-10-45-29.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-45-29.png)

Classic 模式

![reddit-analysis-2019-4-28-10-46-14.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-46-14.png)

对帖子排序

![reddit-analysis-2019-4-28-10-46-29.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-46-29.png)

对帖子排序

![reddit-analysis-2019-4-28-10-46-56.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-46-56.png)

一个帖子能否吸引人，一是内容质量是否高，另一个是图文并茂。

事实上为了缓解大量图片和浏览给 Reddit 服务器带来的压力，很多帖子的图片或短视频是存在 Imgur（一个 Reddit 用户制作的图片分享类应用）上的。

大部分帖子都是带图或 GIF 的，有的 subreddit 会要求提交的帖子必须带图片或短视频，只有部分 subreddit 如 r/ShowerThoughts 以文字为主。

Reddit 在细节上为用户考虑的很周到，默认短视频播放时是静音的，非必要的看帖选项尽量在屏幕上占据最少的位置，而且产品设计上让默认选项几乎满足所以用户日常阅读需求，无需调整。

在产品迭代上 Reddit 也考虑到了老用户的习惯，在看帖模式选项中提供了 Classic（经典）模式。

Reddit 对帖子的排序方式非常有趣，默认是 Hot 即按热度，而 Best 是按照帖子质量提供算法推荐。

将 Best 排在第一位可见 Reddit 是希望用户选择 Best 的，但是人天性是喜欢凑热闹，所以 Hot 才是默认排序。

New 是简单粗暴地按时间排序，Top 是按时间段排序，Controversial 则是赞和踩的数量差不多的争议性帖子，整个排序方式体现出 Reddit 尽力想照顾所有用户的口味。

**回帖 / 评论**：

![reddit-analysis-2019-4-28-10-49-7.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-49-7.png)

![reddit-analysis-2019-4-28-10-49-26.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-49-26.png)

![reddit-analysis-2019-4-28-10-54-34.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-54-34.png)

评论也是能排序的 

![reddit-analysis-2019-4-28-10-54-48.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-54-48.png)

进入某个 subreddit 去看帖

![reddit-analysis-2019-4-28-10-55-11.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-28-10-55-11.png)

在帖子内，高质量评论理所当然是排在前面的，这一点和知乎的高质量回答很像。页面里特别显眼的是“下一条评论”按钮，点击后直接跳到下一个评论。这是一个非常人性化的设计，因为头几条评论下会跟着很多回复，Reddit 采用一种分层结构来展示评论和其回复（以及回复的回复）的关系，回帖叫做“Thread”，回复是“Reply”，两者外观上很相似，因此很容易分不清哪个是评论哪个是回复。

此外，用户也可以像也可以像使用知乎里的“折叠回答”一样折叠当前评论（Collapse thread）。进 subreddit 看帖和百度贴吧的进某个吧看帖类似，但是 Reddit 在展示订阅的 subreddit 的同时，提供了一个优先展示收藏的 subreddit 的区域，这种设计避免了用户在一堆 subreddit 里特意找几个经常逛的 subreddit 的情况，但是也有可能让用户形成局限于浏览前面几个收藏的 subreddit 的习惯。

#### 3.2 精准的算法推荐和搜索

**推荐社区：**

主页里 Home 中呈现的相关推荐社区

![reddit-analysis-2019-4-29-9-15-17.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-15-17.png)

主页里 Home 中呈现的相关推荐社区    

![reddit-analysis-2019-4-29-9-15-41.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-15-41.png)

主页里 Home 中呈现的相关推荐社区    

![reddit-analysis-2019-4-29-9-16-7.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-16-7.png)

得益于强大的算法，Reddit 主页的 Popular 和 Home 每天都能推荐给用户几乎不重复的内容，同时会在 Home 里为用户推荐未订阅的 subreddit。

笔者个人的使用感受是，Reddit 收集的主要是用户浏览记录和关注的 subreddit，不像知乎一样会为某个话题贴上标签并按标签分类，因为某个 subreddit 本身的主旨已经缩小了用户兴趣范围。例如图中的 r/TheWayWeWere, 很明显是在笔者频繁地浏览 r/OldSchoolCool 和 r/HistoryPorn 后生成的推荐结果。

Reddit 非常注重交互的细节和用户体验，避免重复操作或因功能较多找不到入口，图中点击”PREVIEW”后并非简单地跳转到 r/TheWayWeWere 这个 subreddit, 而是用立体卡片模式展现推荐的几个社区：下划卡片将呈现 subreddit 页面的内容，但是并非直接跳转到那个 subreddit；上划将返回“Recommended communities”，可以左划或右划浏览别的卡片。每个推荐社区底部的“SUBSCRIBE”按钮的颜色甚至都不一样，可见几乎每个细节上 Reddit 都希望带给用户新鲜感。

**广告：**

伪装成帖子的广告

![reddit-analysis-2019-4-29-9-18-35.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-18-35.png)

为了避免影响用户体验，Reddit 的广告都像上图一样伪装成了帖子，只有普通帖子显示 subreddit 名字的地方替换成了“PROMOTED”。

知乎的推荐标签下的广告也形似话题，但是广告内容不像 Reddit 明显针对用户习惯，此外 Reddit 的广告密度较低，主要隐藏在某个 subreddit 中伪装成帖子，在 Home 和 Popular 中较少，这样也利于投放方提高广告命中率。

由于笔者经常逛游戏、编程相关的 subreddit，最近 Reddit 也频繁推荐卖外设（如上图）、卖周边以及在线 IT 教育的广告。

Reddit 单条帖子内容里是没有广告的，毕竟高质量帖子才是用户希望看到的，相比之下百度贴吧里在回帖中穿插广告的行为非常影响用户体验。

**搜索：**

根据相关性搜索

![reddit-analysis-2019-4-29-9-19-38.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-19-38.png)

根据相关性搜索

![reddit-analysis-2019-4-29-9-19-58.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-19-58.png)

搜索结果也能分类排序

![reddit-analysis-2019-4-29-9-20-19.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-20-19.png)

搜索关键词优先呈现的是社区，其次是帖子，搜索结果主要根据帖子热度，默认按相关性搜索，搜索结果则优先展示相关社区，其次展现内容相关的帖子。

Reddit 的搜索结果非常神奇，比如上图中按“soccer”搜索中呈现的“埃隆马斯克派迷你潜艇营救泰国被困少年足球队”的新闻，其实和足球运动没什么关系，但是它是这几天全世界都关注的新闻事件，因此也被作为搜索结果命中，并在头几条结果中呈现。

Reddit 说到底并非纯社交平台，在 Google Play 上 Reddit 是分类在“新闻杂志类”应用里的。热点新闻最能吸引评论，阅读和评论带来用户，物以类聚，人以群分，搜索结果的优化体现了这一点。

#### 3.3 极其严格的内容管控和灵活的社区运营

**社区规定和发帖限制：**

发帖

![reddit-analysis-2019-4-29-9-22-0.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-22-0.png)

发帖

![reddit-analysis-2019-4-29-9-22-13.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-22-13.png)

r/polandball 的规定

![reddit-analysis-2019-4-29-9-22-33.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-22-33.png)

r/aww 的规定

![reddit-analysis-2019-4-29-9-22-58.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-22-58.png)

提醒给帖子加标签

![reddit-analysis-2019-4-29-9-23-23.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-23-23.png)

得益于严格的官方管理，类似百度贴吧里“爆吧”的行为很难发生。

Reddit 的 subreddit 管理有两层，最高层是 Reddit 官方，次一层是 moderator 即版主。和百度贴吧不同的是，Reddit 给予版主非常大的自由度。为了维护社区秩序，版主对违禁用户的惩罚力度很强，违规话题或是网络暴力很快会被封禁。

发帖内容有严格的规定，在其他社区里条约和规定内容可能都是被用户拉到最底下直接点“我接受”的，但是在 Reddit 官方会三番五次醒目地显示规定，发帖后还会以站内信形式通知用户遵守规定。

笔者的感受是，官方的严格运营既保证了整体的高质量内容，也维持了高质量的用户，因为恶劣用户一经发现几乎是永久封禁，有的人肉行为甚至会被采取法律行动。

Reddit 环境良好的社区和高质量用户内容吸引了新的希望提升自我、满足社交需求的用户，进而形成一个正循环。背靠庞大的英语母语人口甚至包括欧洲非英语人口，自然而然地就有流量。国内的社区讨论平台如知乎就开始转向“流量为王”的策略，用户变得良莠不齐，产品评论也每况愈下。

发帖后官方会用站内信通知用户给帖子加上适当标签（例如“NSFW”，不适合工作时间阅读，或者“Spoiler”，剧透）。笔者看来这是一个很好的设计，如果发帖时就提醒加标签，可能会让用户厌烦，因为用户可能不太拿捏得准是否该加标签，甚至可能降低用户发帖意愿。这里的加标签不是像知乎一样由系统加关键词搜索推荐生成，即按广泛的内容划分标签（诸如“编程”，“教育”，“恋爱”等），而是由各个 subreddit 自己规定，例如在 r/totalwar 这个全面战争系列的游戏 subreddit 下，会有游戏厂商人员进驻，由他们按游戏作品分代编辑不同标签。这样的设计使得版块内的帖子内容更精准化，细分了用户兴趣。

Reddit 官方极其重视网络欺凌和人肉问题，一旦发现几乎都是严惩，有时候连带 subreddit 一起封禁而不管 subreddit 有多少人订阅，百度贴吧里的人肉、对骂现象对于 Reddit 官方和版主们来说是无法接受的。

**活动：**

“灭霸粉丝清除计划” 

![reddit-analysis-2019-4-29-9-25-57.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-25-57.png)

“灭霸粉丝清除计划” 

![reddit-analysis-2019-4-29-9-26-18.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-26-18.png)

灭霸本人现身

![reddit-analysis-2019-4-29-9-26-33.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-26-33.png)

Reddit 官方非常配合社区用户自发组织的活动。上图的“粉丝清除计划”是七月份由《复仇者联盟 4》的电源粉丝自娱自乐组织的活动，意在模仿电影中灭霸打响指灭掉一半生命的举动。Reddit 官方特意为 r/thanosdidnothingwrong 的版主们开发了随机封禁一般用户的功能，而灭霸演员本人甚至还发来了支持活动的短视频。在国内的社区平台诸如知乎、百度贴吧里，官方活动很多是促销、竞猜，至少笔者没见过为了用户活动单独推出新功能的举动。

## 三．用户意见

近三个月用户评论（Sensor Tower）  

![reddit-analysis-2019-4-29-9-27-45.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-27-45.png)

总评论数（SensorTower）  

![reddit-analysis-2019-4-29-9-28-2.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-28-2.png)

最近评论（Sensor Tower）  

![reddit-analysis-2019-4-29-9-28-23.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-28-23.png)

迅速的迭代

![reddit-analysis-2019-4-29-9-28-46.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-28-46.png)

Reddit 在 Google Play 里的评分是 4.6，用户评论多集中在五星。笔者翻了数页评论找到的几条非五星评论都是反映连上 Wifi 后 app 仍无法登陆或无法加载内容的问题，算是应用快速迭代过程中产生的 bug，而非产品内容。

Reddit 整个产品，包括 Google Play 和 App Store 上的迭代都非常迅速，最近两周内就有 5 个版本，主要修复交互流程方面的 bug, 也优化了搜索内容结果。

## 四．如果我是 PM

### 1. 改进显示发帖、回帖用户身份

用户身份只显示 ID

![reddit-analysis-2019-4-29-9-30-2.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-30-2.png)

事实上 Reddit 是支持自定义头像的，但是帖子中对用户身份的显示可以说很简陋，无论是发帖用户还是回帖用户都只用浅灰色显示其 ID。笔者猜测 Reddit 是故意这样设计，以让用户专注于 subreddit 内的帖子内容和讨论，而非追随单个用户产生的内容，形成”Reddit 大 V”的现象。但是 Reddit 完全可以增加显示用户头像，因为自定义图片比纯文字更容易辨别，同时也让别的用户更容易查看发帖、回帖者个人资料或加好友，目前的纯文字 ID 由于占据屏幕像素较少，在触屏点击查看用户资料时不太方便。

### 2. 改进回帖 / 回复关系

这个评论的回复多达 35 条，缩进方式像是编写代码  

![reddit-analysis-2019-4-29-9-31-16.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-31-16.png)

个人观点，在阐明回复关系上 Reddit 可以采用知乎的形式，在一个回答后可以展开评论，在评论里以文字注明“谁在回复谁”。目前 Reddit 的回复设计源自很久以前的网站时期（2005~2010），这种类似文件夹展开的方式在视觉上会给新用户带来困惑，同时也是 Reddit 长久以来的特色，有利有弊。

### 3. 加强私聊、好友功能

没什么人气的聊天室

![reddit-analysis-2019-4-29-9-32-9.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-32-9.png)

一下午只有几个用户发言

![reddit-analysis-2019-4-29-9-32-22.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-32-22.png)

只有发起聊天时能找到好友  

![reddit-analysis-2019-4-29-9-32-36.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-32-36.png)

Reddit 中的聊天室功能在笔者看来比较鸡肋，上图中的 r/Europe 是一个有多达 176 万订阅的大 subreddit，但是整个下午其社区聊天室就没几个人。在 Chat 标签下 Reddit 默认显示的是 Rooms，但在笔者看来应该优先显示 Direct，加强私聊，并增加一个和 Rooms、Direct 同级别的标签页以显示好友列表（寻找好友需要点击右上角的“+”按钮）。

目前寻找好友并发起聊天需要四个步骤：切到 Chat、点“+”、选好友、发起聊天，操作步骤过深意味着用户不愿意使用。

### 4. 新增“最近浏览社区”功能

其实之前的版本中 Reddit 在“关注的 subreddit”里是有这个功能的，无论订阅与否，用户最近访问过的几个 subreddit 都会以缩略图形式在“FAVORITES”上方显示，不知为何新版本取消了这个功能，猜测是不希望用户形成浏览惯性，导致随手订阅了一堆 subreddit 结果每天只逛那么两三个地方。

## 五、盈利模式

### 1. 广告

Reddit 广告收费基于每千人浏览数，从 $5、$0.75 到 $20000 不等，广告收入是目前 Reddit 的主要收入。

### 2.Reddit Gold 会员

Reddit Gold 收入统计 （feedough.com）  

![reddit-analysis-2019-4-29-9-34-30.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/reddit-analysis-2019-4-29-9-34-30.png)

Premium membership (Reddit Gold) 是 Reddit 推出的提供更好的用户体验的会员模式，不过绝大多数用户还是满足于用免费版，对于 Reddit 这样大规模的社交新闻类平台来说，Reddit Gold 在 2016 年带来的收入只有 96 万美元。

### 3. 周边

得益于随处可见的 Reddit logo （用户默认头像就是）——Reddit 红眼小外星人 Snoo，Reddit 官方有一个销售周边的平台，主要产品是 Snoo 玩偶，Reddit 衬衫等。

## 六、总结

Reddit 在英文世界的流行主要得益于其简洁的界面、流畅的操作流程、高质量的用户产生内容和严格的社区管理。

Reddit 在内容产生上不局限于英文媒体，笔者在热帖中经常能看到抖音短视频，可见 Reddit 的开放性。

Reddit 也在不断探索新功能，但是大多局限于操作细节上的优化，大的功能改动很少，也许是担心大改动引发老用户流失。

盈利模式上，相比较知乎和百度贴吧的迅速变现，Reddit 自 2005 年以来商业化缓慢，但优先保证了用户体验，因此长期来说用户口碑很好。对于国内类似的社交、评论、新闻类应用来说，完全可以学习 Reddit 的“依靠高质量内容和良好运营吸引用户”而非“依靠人头数不管用户质量”的策略，虽然后者可能拥有更庞大的用户基数，但是针对同样的广告，可能前者效果更好，并且不至于引发用户反感。

社交新闻类应用应该专注于平台宗旨，而不是看什么热就上什么板块，诸如短视频、游戏直播，最终导致产品不伦不类，老用户流失。

## 参考链接

[产品分析：美国贴吧 Reddit](https://www.jianshu.com/p/c49ede0fa68a)
