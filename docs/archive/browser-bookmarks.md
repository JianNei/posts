# 地址栏

## 工具

### Sublime Text

* [Sublime text 3 中文文件名显示方框怎么解决](https://zhidao.baidu.com/question/919460697186253539.html)
* [Powerful Markdown package for Sublime Text ](https://github.com/SublimeText-Markdown/MarkdownEditing)

### npm、yarn

* [国内镜像加速npm和yarn](https://blog.csdn.net/u011083967/article/details/74936588)
* [淘宝 NPM 镜像](http://npm.taobao.org/)


### 前端工具

* [腾讯全端 AlloyTeam 团队 Blog - 【腾讯Web前端工具系列1】Live Reload – Chrome扩展](http://www.alloyteam.com/2012/05/dev-tools-chrome-live-reload/)


### 时序图

* [时序图](https://www.websequencediagrams.com/)


### API

* [极客专属的接口协作管理工具](https://apizza.net/pro/#/)
* [小幺鸡，简单好用的接口文档管理工具](http://www.xiaoyaoji.cn/)


### vscode

* [vscode设置中文，设置中文不成功问题](https://www.cnblogs.com/shapaozi/p/9651168.html)
* [专为 Laravel 定制的 Visual Studio Code 编辑器](https://learnku.com/laravel/t/17409)
* [快速入门：使用 Visual Studio 创建第一个 Vue.js 应用](https://docs.microsoft.com/zh-CN/visualstudio/javascript/quickstart-vuejs-with-nodejs?view=vs-2017)
* [【VSCode】格式化缩进代码](https://blog.csdn.net/u013451157/article/details/79502754)
* [如何优雅地使用 VSCode 来编辑 vue 文件？](https://segmentfault.com/a/1190000008749631)
* [在 vscode 中写 Markdown 如何装X](https://juejin.im/post/5c45b92751882525487c5c66)
* [markDown编辑必备插件](https://www.kancloud.cn/pwedu/code-fans/321504)

### phpstorm

* [IntelliJ Idea插件连不上网问题](https://blog.csdn.net/love905661433/article/details/84074685)

### webstorm

* [eslint的配置和使用（2018版）](https://blog.csdn.net/qq_29329037/article/details/80100450)
* [webstorm中配置Eslint的两种方式及差异比较](https://blog.csdn.net/u014390748/article/details/79477652)
* [用vue-cli构建的项目(开启了eslint)，webstorm的自动格式化与eslint冲突报错](https://segmentfault.com/q/1010000013824902)
* [WebStorm如何即时显示更改内容和热更新](https://zhangjia.io/574.html)


### 在线差异比对

* [在线差异比对](http://www.jq22.com/textDifference)

### 代码截图

* [代码截图](https://carbon.now.sh)


### win 10 使用

* [windows 10常用快捷键 win10常用快捷键](https://www.jianshu.com/p/12c9358b55cf)
* [双屏工作，开启你的高（zhuang）效（bi）时代 。](https://zhuanlan.zhihu.com/p/27081833)

### markdown

* [程序员必备软件之编辑预览于一身的 Typora](https://juejin.im/post/5aacd628f265da239b413457)
* [使用 Typora 一次性搞定公众号写作与排版](https://sspai.com/post/40524)
  
### 微信公众号

* [新浪短网址](http://www.sina.lt/)
* [微信公众号格式化工具](http://blog.didispace.com/tools/online-markdown/)
* [可能吧公众号 Style 一键转换器](https://knb.im/mp/)
* [如何将Markdown文章轻松地搬运到微信公众号并完美地呈现代码内容](http://blog.didispace.com/markdown-convert-to-weixin/)
* [emoji](https://emojipedia.org/)
* [二维码设计](https://www.chuangkit.com/sj-pi1-si41-or1-pt2-cr0-pn2.html)

### 命名

* [codelf](https://unbug.github.io/codelf/)

### 代码在线运行

* [php 在线运行](https://tool.lu/coderunner/)


### 阿里云

* [阿里云 ECS 服务器 Ubuntu14.04 部署 Laravel 5.5 项目上线](https://learnku.com/articles/8350/ali-cloud-ecs-server-ubuntu1404-deploys-laravel-55-project-online)
* [Laravel 部署到阿里云 / 腾讯云](https://learnku.com/articles/8983/laravel-deploys-to-the-ali-cloud-tencent-cloud)
* [linux下添加用户并赋予root权限](https://www.cnblogs.com/albert-think/p/8479209.html)
* [从零开始部署一个 Laravel 站点](https://www.codecasts.com/blog/post/deploy-laravel-app-on-ubuntu-vps)
* [oneinstack](https://oneinstack.com/auto/)
* [又一篇 Deployer 的使用攻略](https://learnku.com/articles/13242/another-introduction-to-the-use-of-deployer)
* [Piplin(灵感来自于"pipeline"，读作/ˈpɪpˌlɪn/ 或 /ˈpaɪpˌlaɪn/)是一款免费、开源的持续集成与部署系统，适用于软件的自动化构建、测试和部署相关的各种应用场景。](https://github.com/piplin/piplin)
* [阿里云MySQL远程连接不上问题](https://www.cnblogs.com/funnyboy0128/p/7966531.html)
* [deployer](https://deployer.org/docs/configuration.html)
* [一步一步教你部署自己的Laravel应用程序到服务器](https://lufficc.com/blog/step-by-step-teach-you-to-deploy-your-laravel-application-to-server)
* [如何在Ubuntu 16.04上使用Deployer自动部署Laravel应用程序](https://www.howtoing.com/automatically-deploy-laravel-applications-deployer-ubuntu)
* 宝塔Linux面板
* [面板账号和密码的修改](http://docs.bt.cn/424370)

### php 

* [2017 年 PHP 程序员未来路在何方](https://zhuanlan.zhihu.com/p/26090126)
* [php-the-right-way](https://github.com/codeguy/php-the-right-way)
* [php-the-right-way 中文版](http://laravel-china.github.io/php-the-right-way/)
* [2018年新版PHP学习路线图（内含大纲+视频+工具+教材+面试）](http://www.itheima.com/news/20180521/133654.html)
* [codecasts 从高质量的视频中学习 Web 开发技术](https://www.codecasts.com/)
* [推荐一个 PHP 网络请求插件 Guzzle](https://www.ctolib.com/topics-125710.html)
* [Web 金字塔式开发框架分层模型概述](https://learnku.com/articles/5813/overview-of-hierarchical-model-for-web-pyramid-development-framework)
* [RESTful API 设计指南](http://www.ruanyifeng.com/blog/2014/05/restful_api.html)
* [怎样用通俗的语言解释REST，以及RESTful？](https://www.zhihu.com/question/28557115)
* [理解HTTP幂等性](http://www.cnblogs.com/weidagang2046/archive/2011/06/04/2063696.html)
*  [Swagger 文档](http://phpboot.org/zh/latest/advanced/docgen.html#swagger)

## 官方

### Web 开发技术

* [Web 开发技术](https://developer.mozilla.org/zh-CN/docs/Web)
* [ECMAScript® 2020 Language Specification](https://tc39.github.io/ecma262/)
* [ECMAScrpit6 特性浏览器兼容清单](https://kangax.github.io/compat-table/es6/)
* [UI Events](https://www.w3.org/TR/uievents/)


### Chrome

* [Chrome 开发者工具](https://developers.google.com/web/tools/chrome-devtools/)
* [开发者专用的每日构建版](https://www.google.com/chrome/canary/)

### Laravel

* [Laravel 5.1 LTS 速查表](https://cs.laravel-china.org/)


### 网络优化

*  [centos下dnsmasq安装与配置](https://www.cnblogs.com/wclwcw/p/8806256.html)
*  [Linux中通过缓存DNS的解析来提高上网的响应速度！](https://blog.csdn.net/chenguoda/article/details/2524168)
*  [Linux命令大全：dig命令网络测试](http://man.linuxde.net/dig)
*  [php apc缓存以及与redis的对比](https://blog.csdn.net/w18704622664/article/details/47132061/)
*  [原因：curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));](http://www.360doc.com/content/15/0531/00/18139076_474547548.shtml)
*  [curl时设置Expect的必要性](https://www.jianshu.com/p/22468ac7048d)

### Laravel

* [Laravel5.5+Vue+Element-ui+Vux 环境搭建](https://blog.csdn.net/mrwangweijin/article/details/78126714)
* [快速构建 RESTful API 以及现代化 Web 应用的 Laravel 原型项目 —— Someline](https://laravelacademy.org/post/6408.html)
* [使用 Clockwork 扩展在 Chrome 浏览器中显示 Laravel 应用调试信息](https://laravelacademy.org/post/3746.html)
* [Laravel5.5+passport 放弃 dingo 开发 API 实战，让 API 开发更省心](https://learnku.com/articles/6035/laravel55-developing-api-combat)
* [Laravel学习笔记之Errors Tracking神器——Sentry](https://segmentfault.com/a/1190000007055720)
* [Laravel/Swoole 源码分析](https://learnku.com/blog/leoyang)
* [如何理解Laravel 的IOC 服务容器](https://blog.csdn.net/oZuoAZuo/article/details/60955155)
* [理解依赖注入与控制反转](https://learnku.com/laravel/t/2104/understanding-dependency-injection-and-inversion-of-control)
* [Laravel 学习笔记 —— 神奇的服务容器](https://learnku.com/articles/789/laravel-learning-notes-the-magic-of-the-service-container)
* [Sentry 自动化异常提醒](https://learnku.com/articles/4235/sentry-automation-exception-alert)
* [一步一步学Laravel，在phpstorm中快速更新命名空间或者代码提示](https://onlyke.com/html/463.html)
* [Laravel学习笔记八-常用包及用法收集](https://segmentfault.com/a/1190000008621492)
* [laravel 源码解析](https://legacy.gitbook.com/book/leoyang90/laravel-source-analysis/details)
* [andersao/l5-repository实践记录](https://www.jianshu.com/p/d640a61d8631)
* [l5-repository的个人学习版](https://github.com/daofirst/l5-repository)
* [Laravel 5 - Repositories to abstract the database layer](https://github.com/andersao/l5-repository)
* [关于 RESTful API 设计的总结](https://learnku.com/articles/7048/a-summary-of-the-design-of-restful-api)
* [Laravel 的中大型專案架構](https://oomusou.io/laravel/architecture/)
* [使用 Laravel API 文档生成器扩展包自动为项目生成 API 文档](https://laravelacademy.org/post/4578.html)
* [面对现实吧！维护大型 PHP 应用程序不简单！](https://learnku.com/articles/4630/wake-up-and-smell-the-coffee-maintenance-of-large-php-applications-is-not-simple)
* [Laravel 5.5 发布，PJ Blog 升级 & Yike 项目介绍！](https://learnku.com/laravel/t/5863/laravel-55-release-pj-blog-upgrade-yike-project-presentation)
*  [Laravel 做 API 服务端，VueJS+iView 做 SPA，给新手一个 Demo](https://learnku.com/laravel/t/3904/laravel-do-api-server-vuejsiview-do-spa-a-new-demo)
*  [作为前后端开发工程师，我用 Laravel 和 Vue 撸了一个管理后台模版](https://learnku.com/articles/6671/as-the-front-end-development-engineer-i-used-laravel-and-vue-into-a-management-background-template)
*  [Is it possible to include Vue Admin (Bulma) into Laravel?](https://laracasts.com/discuss/channels/vue/is-it-possible-to-include-vue-admin-bulma-into-laravel)


## 优秀博客

* [点灯坊](https://oomusou.io/)


## 开源项目

### Laravel

* [基于Laravel开发,支持markdown语法,简洁的博客系统](https://v2mm.tech/topic/93/moell-blog-%E5%9F%BA%E4%BA%8Elaravel%E5%BC%80%E5%8F%91-%E6%94%AF%E6%8C%81markdown%E8%AF%AD%E6%B3%95-%E7%AE%80%E6%B4%81%E7%9A%84%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F)
* [Moell Blog](https://github.com/moell-peng/moell-blog)
* [yikeio/yike](https://github.com/yikeio/yike)
* [Laravel integration with vue-admin](https://github.com/gazben/laravel-vue-admin)
* [Laravel Vue SPA, Bulma themed.](https://github.com/laravel-enso/enso)


### Vue

* [vue-admin](https://github.com/vue-bulma/vue-admin)
* [vue-bulma,轻量级高性能MVVM Admin UI框架](https://github.com/wangxg2016/vue-bulma)
* [UI Component Library Base on Vue.js(2.x) and Bulma](https://github.com/chenz24/vue-blu)
* [Vue技术内幕](http://hcysun.me/vue-design/art/)
* [深入浅出学 Vue 开发](https://gitbook.cn/gitchat/column/5b5b38219a3b4b3ca3085030)

### JavaScript

* [Javascript 缺陷](http://crockford.com/javascript/)
* [javascript.info](https://javascript.info/)

## 阮一峰

* [Windows 10 PC 安装 Docker CE](https://yeasy.gitbooks.io/docker_practice/content/install/windows.html)
* [webpack-demos](https://github.com/ruanyf/webpack-demos)
* [JavaScript 全栈工程师培训教程](http://www.ruanyifeng.com/blog/2016/11/javascript.html)
* [ruanyf/react-demos](https://github.com/ruanyf/react-demos)


## 待整理书签

* [电影下载](http://www.hao6v.com/)
* [电影下载](hhttp://www.66s.cc/)
* [石墨文档](https://shimo.im/)
* [声享，在线编辑 PPT](https://ppt.baomitu.com/#2)
* [Python 教程，廖雪峰](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
* [语雀，专业的云端知识库](https://www.yuque.com/)
* [awesomes web](https://www.awesomes.cn/)
* [Airbnb JavaScript 编码规范](https://github.com/yuche/javascript)
* [Docker_入门？只要这篇就够了！（纯干货适合0基础小白）](https://blog.csdn.net/S_gy_Zetrov/article/details/78161154)
* [I love you ](https://ncase.me/door/#%E6%88%90%E7%88%B0%E4%BD%A1%EF%BC%8D%E4%BD%A1%E8%A7%88%E5%BE%96%E6%9C%88%E5%8F%AE%E8%83%BC%E4%B9%89%EF%BC%9E)
* [php-curl（模拟post，设置header，接收json数据）](https://blog.csdn.net/ljl890705/article/details/52219565)
* [git 教程](https://git-scm.com/book/zh/v2)
* [阮一峰的网络日志](http://www.ruanyifeng.com/blog/archives.html)
* [蓝湖，高效的设计协作平台](https://lanhuapp.com/web/?code=011EXBlY0DAQtV1urnnY0ZMqlY0EXBlr&state=#/user/login)
* [keenthemes](https://keenthemes.com/metronic/preview/?page=components/icons/fontawesome5&demo=default)
* [VPN](https://1840o.com/main.php)
* [vue-blu](https://github.com/chenz24/vue-blu)
* [bulma](https://bulma.io/documentation/overview/start/)
* [V01 Vue.js 实战教程 - 基础篇](https://learnku.com/courses/vuejs-essential)
* [Laradock 中文文档](https://gitee.com/xmkc/laradock)

### Linux

* [sudo时出现unable to resolve host 的解决方法](https://blog.csdn.net/skh2015java/article/details/80152730)
* [ubuntu 重启 nginx 失败，* Restarting nginx nginx ...fail!](https://www.cnblogs.com/php-linux/p/6018301.html)
* [ubuntu如何彻底删除用户（帐户）](https://blog.csdn.net/Joker_Mr/article/details/78225260)

### git

* [Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)
* [优雅的提交你的 Git Commit Message](https://juejin.im/post/5afc5242f265da0b7f44bee4)

### css

* [chokcoco/iCSS](https://github.com/chokcoco/iCSS)

### 简历/面试

* [PHP 高级工程面试题汇总](https://learnku.com/articles/20714)
* [一个 16年毕业生所经历的 PHP 面试](https://learnku.com/articles/6844/a-php-interview-for-a-16-year-old-graduate)
* https://github.com/OMGZui/noteBook
* [php 7 面试题](https://www.google.com/search?q=php+7+%E9%9D%A2%E8%AF%95%E9%A2%98&oq=php+7+%E9%9D%A2%E8%AF%95%E9%A2%98&aqs=chrome..69i57.8592j0j7&sourceid=chrome&ie=UTF-8)
* [PHP 详细面试总结 (每日更新)](https://learnku.com/articles/25204)
* [如何写一份更好的简历](https://www.enginego.org/%E5%9F%BA%E7%A1%80/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98/%E5%A6%82%E4%BD%95%E5%86%99%E4%B8%80%E4%BB%BD%E6%9B%B4%E5%A5%BD%E7%9A%84%E7%AE%80%E5%8E%86/)
* [超级简历](https://www.wondercv.com/zh-CN/cvs)
* [程序员职业小白书 —— 如何规划和经营你的职业](https://juejin.im/book/59e17a7ff265da430629cc4e/section/59faec225188252abc5db42c)
* [PHP程序员简历模板](https://github.com/geekcompany/ResumeSample/blob/master/php.md)

### vuepress

* [超详细动手搭建一个Vuepress站点及开启PWA与自动部署](https://segmentfault.com/a/1190000014746656?utm_source=tag-newest#articleHeader0)
* [VuePress从零开始搭建自己专属博客](https://blog.csdn.net/codeteenager/article/details/81807810)
* [awesome-vuepress](https://github.com/ulivz/awesome-vuepress)
* [手摸手教你用VuePress打造个人Blog](https://gongjs.github.io/vuepress/)
* [vuepress-blog](https://github.com/hirCodd/vuepress-blog)
* [VuePress 手摸手教你搭建一个类Vue文档风格的技术文档/博客](http://obkoro1.com/2018/09/09/VuePress-%E6%89%8B%E6%91%B8%E6%89%8B%E6%95%99%E4%BD%A0%E6%90%AD%E5%BB%BA%E4%B8%80%E4%B8%AA%E7%B1%BBVue%E6%96%87%E6%A1%A3%E9%A3%8E%E6%A0%BC%E7%9A%84%E6%8A%80%E6%9C%AF%E6%96%87%E6%A1%A3-%E5%8D%9A%E5%AE%A2/)
* [我的这套VuePress主题你熟悉吧](https://juejin.im/post/5c6dc6465188256559172860)
* [vuepress-theme-indigo-material](https://github.com/zhhlwd/vuepress-theme-indigo-material)
* [vuepress-theme-reco](https://github.com/recoluan/vuepress-theme-reco)
* [一步步搭建 VuePress 及优化【插件系列】](https://segmentfault.com/a/1190000018715170)
* [remote: error: GH007: Your push would publish a private email address.](https://blog.csdn.net/jingfengvae/article/details/72859130)
* [通过TravisCI自动化部署VuePress到GithubPages](http://gdzrch.win/post/Deploy-Vuepress-Blog-On-TravisCI.html)
* [github 上搭建个人博客的问题](https://segmentfault.com/q/1010000000585417/a-1020000000585465)
* [GitHub教程 如何修改Github用户名](https://blog.csdn.net/qq_36667170/article/details/79067306)
* [一步步搭建 VuePress 及优化【插件系列】](https://juejin.im/post/5c9f26c96fb9a05e6d61a1b4)
* [VuePress博客搭建笔记（二）个性化配置](https://segmentfault.com/a/1190000017953711#articleHeader8)
* [yarn 管理依赖](https://yarnpkg.com/lang/zh-hans/docs/managing-dependencies/)
* [使用Travis-ci自动SSH部署代码](https://www.zhuwenlong.com/blog/article/5c24b6f2895e3a0fb4072a5c)
* [一点都不高大上，手把手教你使用Travis CI实现持续部署](https://zhuanlan.zhihu.com/p/25066056)
* [You need to specify at least one host or stage. use inventory('hosts.yml');](https://github.com/deployphp/deployer/issues/1393)
* [如何使用 VuePress 编写静态博客](https://www.unaxu.com/blog/posts/005-one-how-to-generate-static-blog-with-vuepress.html#%E5%AE%89%E8%A3%85%E4%B8%BB%E9%A2%98%E6%8F%92%E4%BB%B6)
* [利用Gitalk给Vuepress搭建的blog增加评论功能](https://www.jianshu.com/p/b883c9867a77?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation)
* [在 Vuepress 中使用 Vssue](https://vssue.js.org/zh/guide/vuepress.html)
* [vuepress-theme-bulma](https://github.com/nakorndev/vuepress-theme-bulma/blob/master/demo/.vuepress/theme/Layout.vue)
* https://github.com/search?q=vuepress
* [vuepress-theme-meteorlxy](https://vuepress-theme-meteorlxy.meteorlxy.cn/posts/2019/02/26/theme-guide-zh.html)
* [如何简单入门使用Travis-CI持续集成](https://github.com/nukc/how-to-use-travis-ci)
* [跟踪 Github 项目的持续集成状态](https://harttle.land/2016/04/30/github-ci.html)

#### markdown

* [在 Windows 上拥有舒适的码字体验，12 款 Markdown 写作工具推荐](https://sspai.com/post/42126)
* [vue markdown编辑器](https://www.jianshu.com/p/f4eb56539aa7)
* [Editor.md 开源在线 Markdown 编辑器](https://pandao.github.io/editor.md/)
* [五分钟搭建一个MarkDown文档解析器](https://my.oschina.net/lsjcoder/blog/1603414)
* [laravel markdown](https://github.com/search?o=desc&q=laravel+markdown&s=stars&type=Repositories)
* [作为一个专业写手，你怎么能用 Typora ？？？](https://www.imooc.com/article/42587)
* [【币乎一键排版】笑来老师风格](https://bihu.com/article/63936)
  
  
#### 资源

* [iconfinder](https://www.iconfinder.com/)
* [Favicon Generator. For real.](https://realfavicongenerator.net/)
* [免费插图提升您的项目](https://icons8.cn/ouch)


### Laravel 社区文档

* [Laravel 5.8 中文文档](https://learnku.com/docs/laravel/5.8)
* [Laravel 项目开发规范](https://learnku.com/docs/laravel-specification/5.5)
* [Laravel 开发环境部署](https://learnku.com/docs/laravel-development-environment/5.8)
* [Laravel Nova 中文文档](https://learnku.com/docs/nova/1.0)
* [Lumen 中文文档 5.7](https://learnku.com/docs/lumen/5.7)
* [Dingo API 2.0.0 中文文档](https://learnku.com/docs/dingo-api/2.0.0)
* [深入 Laravel 核心](https://learnku.com/docs/laravel-core-concept/5.5)
* [Laravel Mix 中文文档](https://learnku.com/docs/laravel-mix/4.0)
* [Laravel 之道](https://learnku.com/docs/the-laravel-way/5.6)
* [Vue 2 入门学习笔记](https://learnku.com/docs/learn-vue2)
* [Testing Laravel 单元测试入门笔记](https://learnku.com/docs/phpunit-testing-laravel)
* [社区使用指南](https://learnku.com/docs/guide)
* [GraphQL PHP 中文文档](https://learnku.com/docs/graphql-php)
* [TDD 构建 Laravel 论坛笔记](https://learnku.com/docs/forum-in-laravel-tdd)

### 其他

* [kubernetes 中文社区](https://www.kubernetes.org.cn/)
* [laradock](http://laradock.io/)
* [一个 UI 组件库](https://rsuite.gitee.io/#/components/table)
* [关于HeyUI](https://www.heyui.top/component/guide)
* [javascript权威指南第六版，第七版（包含源码）](https://github.com/wanlixi/javascript-learn-book)
* [收集的各种资源](https://github.com/mynane/PDF)
* [layui](https://www.layui.com/doc/)
* [DocumentFragment 的优化小知识](https://juejin.im/post/590f4eadac502e006cf718c3)
* [JavaScript控制浏览器全屏显示简单示例](https://www.jb51.net/article/143207.htm)
* [现代 JavaScript 教程](https://zh.javascript.info/)
* [JavaScript 教程](https://wangdoc.com/javascript/) 
* [vue 全家桶](http://doc.liangxinghua.com/vue-family/1.html)
* [[译]如何在 Vue.js 中使用第三方库](https://github.com/dwqs/blog/issues/51)
* [Electron-vue开发实战0](https://juejin.im/post/5a572f26f265da3e513305f6)
* [awesome-vscode](https://github.com/viatsko/awesome-vscode)
* [PHPUnit单元测试对桩件（stub）和仿件对象（Mock）的理解](https://blog.csdn.net/loophome/article/details/52198716)
* [基于 WEB 的 WMS 3D 可视化管理系统](https://www.cnblogs.com/iotopo/p/wms3d.html)
* [PHP 生成随机字符串与唯一字符串](https://www.cnblogs.com/dee0912/p/5240370.html)
* https://codepen.io/omercanbalandi/pen/EGqogq
* [php-awesome](https://github.com/shockerli/php-awesome)
* [nvm](https://github.com/creationix/nvm)
* [关于html name 和id，Form input without an associated label or title attribute](https://www.cnblogs.com/John-/p/6985872.html)
* [cn_windows_10](https://www.google.com/search?ei=TS25XJrcGpus0PEP48GAmAg&q=cn_windows_10&oq=cn_windows_10&gs_l=psy-ab.3..0.183919.192671..193112...0.0..1.841.7176.2-5j3j1j2j4....2..0....1..gws-wiz.......35i39j0i67j0i10.GfoT4Iw23sk)
* [Windows 10全家桶 2019年4月18日更新：Windows 10 (Business+Consumer), Version 1809 (Updated April 2019)](https://www.itpwd.com/191.html)
* [gitlab 安装](https://www.gitlab.com.cn/installation/)
* [win7笔记本VirtualBox安装黑苹果MacOS 10.13](https://www.cnblogs.com/xiangyuecn/p/8760168.html)
* [VirtualBox 安装Mac OS 10.13 及安装增强功能](https://blog.csdn.net/kaluosifa/article/details/87919734)
* [三分钟教你同步 Visual Studio Code 设置](https://juejin.im/post/5b9b5a6f6fb9a05d22728e36)
* [简明 VIM 练级攻略](https://coolshell.cn/articles/5426.html)
* [Guzzle中文文档](https://guzzle-cn.readthedocs.io/zh_CN/latest/)
* [php RSA加密传输代码示例](https://www.cnblogs.com/firstForEver/p/5803940.html)
* [Laravel 开发插件必备三件套](https://blog.csdn.net/h330531987/article/details/79089657)
* [layui 规范](https://www.layui.com/doc/base/modules.html)
* [全文搜索引擎 Elasticsearch 入门教程](http://www.ruanyifeng.com/blog/2017/08/elasticsearch.html)