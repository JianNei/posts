# Laravel API Development & Vue JS SPA

To get started, let's install a fresh installation of Laravel and get working on setting up our project.

Buy the full course at https://www.udemy.com/laravel-api-dev...

In this episode, we cover:

Scaffolding of new Laravel project

Scaffolding of authentication

Strip all of the default front-end

Install Vue, Vue Router & Tailwindcss using NPM

About the course
Learn how to develop a robust API with Laravel and a Single-Page Application in Vue JS from Scratch

What you'll learn
- RESTful API Development with Laravel
- Vue JS Single Page Application Methodology
- Front-End Design Using Tailwind CSS
- Implementing Search Functionality Using Laravel Scout
- Build a Complete SPA from Scratch

## e01- xx

https://www.youtube.com/watch?v=AFyzK8qohdE

- 安装：`composer create-project laravel/laravel jot --prefer-dist "5.8.*"`
- 修改忽略文件
- `php artisan help make:auth`
- `php artisan help preset`
- `php artisan make:auth`
- `php artisan preset none`
- `php artisan preset vue`
- `yarn add vue-router taiwindcss --dev`