---
home: true
heroImage: /hero.png
actionText: 每日一篇 →
actionLink: /daily/
footer: MIT Licensed | Copyright © 2019-present JianNei
---