# 024 Redis

## 常用命令

* 切换数据库：`select index`
* 查看全部 keys ：`keys *`
* 查看某个Key 过期时间：`ttl key`
* 清楚全部数据库的全部数据：`flushall`

疑问：
redis 使用场景：数据库、缓存、队列

缓存：
* 数据库查询数据缓存
* 接口调用缓存
* 接口响应缓存
* 内部循环体缓存

redis 用作数据库还是用作缓存系统的问题？
* 用作缓存需要在 .env 文件中设置 `CACHE_DRIVER` 为 redis，默认为 file
* 用作缓存，可以使用 `php artisan cache:clear`来清除 redis 中用于缓存的数据库（Laravel 在 database.php 文件中，`'database' => env('REDIS_CACHE_DB', 1),` 默认 Index 为 1 数据库用于缓存）