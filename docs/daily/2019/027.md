# 027 laravel-echo-server

## 时间

2019 年 6 月 5 日。

## 过程

Documentation tells you to globally install “laravel-echo-server” and start it using their CLI tool. This doesn’t sound right but we can make it cleaner.

Instead of installing “laravel-echo-server” globally, you can pull it in as one of your npm dependencies.

```
▶ npm install laravel-echo-server
```

Then create a “server.js” file where you can pass in your options for the server.

```
require('dotenv').config();

const env = process.env;

require('laravel-echo-server').run({
    authHost: env.APP_URL,
    devMode: env.APP_DEBUG,
    database: "redis",
    databaseConfig: {
        redis: {
            host: env.REDIS_HOST,
            port: env.REDIS_PORT,
        }
    }
});
```

Note that I also use a “dotenv” package so I can re-use the “.env” file of my Laravel application which already has all of the variables needed to run the server.(APP_DEBUG, APP_URL, REDIS_HOST, REDIS_PORT, etc.)

And lastly, run “node server.js” to start the server.

If you wish, you can also extract it as a separate npm project where your “package.json” is going to look similar to this:

```
{
    "name": "laravel-echo-server",
    "version": "0.0.1",
    "description": "Laravel Echo Server",
    "scripts": {
        "start": "node server.js"
    },
    "dependencies": {
        "dotenv": "^4.0.0",
        "laravel-echo-server": "^1.2.8"
    }
}
```

In the next post, I’ll show you how to implement a NodeJS Cluster on top of this Socket.io application.

Hope you liked this little trick which makes your dependencies look much cleaner.

## 参考

https://medium.com/@titasgailius/running-laravel-echo-server-the-right-way-32f52bb5b1c8
https://github.com/tlaverdure/laravel-echo-server



