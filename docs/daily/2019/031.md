# 031 Vue-router

* route 与 router 区别
* 获取上一次路由

```
// https://segmentfault.com/q/1010000010947051

获取vue-router的上一个页面是否存在或者是否是自己需要返回的地址，可以使用vue-router的的声明周期函数，有三种模式：
第一种、使用全局函数beforeEach，直接来获取form.path(即为对应的上一次地址的路由path内容)；

 beforeEach(to, from, next) {
    path = from.path //path为定义的变量，不是vue的data定义的变量，当前生命周期data还未初始化
    next()
}
第二种、使用组内共享函数beforeEnter，直接来获取form.path(即为对应的上一次地址的路由path内容)；

 beforeEnter(to, from, next) {
    path = from.path //path为定义的变量，不是vue的data定义的变量，当前生命周期data还未初始化
    next()
}
第三种、使用组件内函数，beforeRouteEnter，，直接来获取form.path(即为对应的上一次地址的路由path内容)；

beforeRouteEnter(to, from, next) {
    path = from.path //path为定义的变量，不是vue的data定义的变量，当前生命周期data还未初始化
    next()
}
```

是否可以考虑使用全局的 vuex 记录全部路由历史？

* 问题：`this.$router.back()` 与 `this.$router.push('/github')`，在使用 github 登录，跳转到 Oauth 页面进行通过授权 code 换取 github 用户信息，获取完以后返回到 github 登录界面提示登录成功

在这个过程中，使用 `this.$router.back()` 会导致通过 code 调用后端API 换取 github 用户信息的请求无法发起？请求还未发起，页面就直接返回了

```
if (this.code) {
    this.$axios.post('/auth/github/login', data).then(response => {
    console.log(response.data)
    window.localStorage.setItem('token', response.data.access_token)
    })
}

// 修改前
// 重定向到原先登录页面
this.$router.back()

// 修改后
this.$router.push('/github')
```
