# 009 Vargant

## 前言

使用 Vagrant 搭建一个与项目实际运行环境一样的开发环境。

## 更新时间

2019 年 05 月。

## 环境信息

| 涉及工具    |      工具版本       |
| ----------- | :-----------------: |
| 操作系统    | Win10  教育版 64 位 |
| Git         |       2.17.0        |
| Virtual box |       8.11.2        |
| vagrant         |        6.9.0        |
| Ubuntu box        |        14.04 64 位        |


## 操作步骤

## 软件下载

* Virtual box
* vagrant
* Ubuntu box

```
vagrant box add Ubuntu ubuntu-14.04.box # 添加 box
vagrant init Ubuntu # 初始化
vagrant up # 启动 box
vagrant ssh # ssh 连接 box
```

* 修改 ubuntu 镜像源：https://jiannei.github.io/github-pages/blog/operation-and-maintenance/system/linux-replace-apt-source.html#%E5%89%8D%E8%A8%80

### 重启

```
vagrant reload --provision
```

### 查看 box

```
vagrant box list
```

### 删除 box

```
vagrant destroy

vagrant box remove lc/homestead
```

### 打包

```
vagrant package --output xxx.box
```

## 搭建环境

PHP 多版本切换可以考虑 宝塔

常规安装：http://www.54php.cn/default/26.html
https://jiannei.github.io/github-pages/weekly/weekly-01.html#%E9%85%8D%E7%BD%AE%E5%B7%A5%E4%BD%9C

## 配置

### 转发端口

```
config.vm.network "forwarded_port", guest: 3306, host: 33060, host_ip: "127.0.0.1"
```

### 私有 ip（只能本地访问)

```
config.vm.network "private_network", ip: "192.168.33.10"
```

### 局域网 / 公网可以访问

固定 Ip

```
config.vm.network "public_network", auto_config: false

# manual ip
config.vm.provision "shell",
run: "always",
inline: "ifconfig eth1 192.168.1.11 netmask 255.255.255.0 up"

# manual ipv6
config.vm.provision "shell",
run: "always",
inline: "ifconfig eth1 inet6 add fc00::17/7"
```

### 同步文件夹

```
config.vm.synced_folder "E:/develop/jiangluan", "/www/wwwroot"
```

### 优化

```
虚拟机名称
vb.name = "ubuntu_mooc" # vitrtaul box 中显示的名称
配置虚拟机内存和 CPU
vb.memory = "1024"
vb.cpus = 2

虚拟机主机名
config.vm.hostname = "mooc" # ssh 登录后的名称
```

## composer 安装

https://pkg.phpcomposer.com/
https://learnku.com/laravel/composer

```
php -r "copy('https://install.phpcomposer.com/installer', 'composer-setup.php');"
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
```

## 使用 Homestead

* 修改 composer 镜像
* 修改 yarn 镜像
*  `compoer global update`

https://learnku.com/docs/laravel-development-environment/5.8

```
vagrant box add metadata.json
cd ~
git clone https://git.coding.net/summerblue/homestead.git Homestead

cd ~/Homestead
git checkout v7.8.0

bash init.sh
```

## 可能遇到问题

### 同步文件失败

```
sudo apt-get update
sudo apt-get install virtualbox-guest-utils
```

### 卡慢

[解决 Windows 系统使用 Homestead 运行 Laravel 本地项目响应缓慢问题](https://learnku.com/articles/9009/solve-the-slow-response-problem-of-windows-system-using-homestead-to-run-laravel-local-project)

### 如何解决在 homestead 虚拟机中通过 SSH Key 提交 github 失败报错！Permission denied (publickey)

http://www.zhongruitech.com/100065694.html

```
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_rsa
```

然后将本地主机中 id_rsa.pub 公钥中的内容复制出来。
再到 GitHub 中添加一个新的 SSH Key

* 局域网 ssh 访问：http://landcareweb.com/questions/202/ru-he-tong-guo-zhu-ji-cong-wai-bu-sshdao-virtualbox-guestxu-ni-ji
  
```
登录 guest 虚拟机 Linux VirtualBox VM 的最佳方法是 转发端口。默认情况下，您应该已经有一个正在使用的接口 NAT。然后去 网络 设置并单击 转发端口 按钮。添加一个新的 规则：

主机端口 3022，来宾端口 22，名称 ssh，其他左侧空白。

或者从命令行

VBoxManage modifyvm myserver --natpf1 "ssh,tcp,,3022,,22"
其中'myserver'是创建的 VM 的名称。检查添加的规则：

VBoxManage showvminfo myserver | grep 'Rule'
就这样！请确保您不要忘记安装 SSH VM 中的服务器：

sudo apt-get install openssh-server
要 SSH 到 guest 虚拟机 VM，请写入：

ssh -p 3022 user@127.0.0.1
哪里 user 是您在 VM 中的用户名。
```

## TODO

考虑使用 docker 作为替代方案

## 参考

https://mirrors.tuna.tsinghua.edu.cn/help/ubuntu/
https://www.cnblogs.com/lyon2014/p/4715379.html
https://learnku.com/docs/laravel-development-environment/5.5/upgrade-homestead-box/2315