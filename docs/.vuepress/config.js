module.exports = {
  "title": "Jiannei 的网络日志",
  "base": "/posts/",
  "description": "学习，记录，分享。",
  "dest": 'public',
  "head": [
    ['link', {rel: 'apple-touch-icon', href: '/apple-touch-icon.png'}],
    ['link', {rel: 'icon', href: '/favicon.ico'}]
  ],
  "locales": {
    "/": {
      "lang": "zh-CN"
    }
  },
  "plugins": [
    [
      "@vuepress/back-to-top",
      true
    ],
    [
      "@vuepress/medium-zoom",
      true
    ]
  ],
  "themeConfig": {
    "nav": [
      {
      "text": "每日一篇",
      "link": "/daily/"
    },
      {
        "text": "每周分享",
        "link": "/weekly/"
      },
      {
        "text": "博客文章",
        "items": [{
          "text": "前端",
          "items": [{
            "text": "Html",
            "link": "/blog/front-end/html/"
          },
            {
              "text": "CSS",
              "link": "/blog/front-end/css/"
            },
            {
              "text": "JavaScript",
              "link": "/blog/front-end/javascript/"
            },
            {
              "text": "工具",
              "link": "/blog/front-end/tool/"
            }
          ]
        },
          {
            "text": "后端",
            "items": [{
              "text": "PHP",
              "link": "/blog/backend/php/"
            },
              {
                "text": "数据库",
                "link": "/blog/backend/database/"
              },
              {
                "text": "框架",
                "link": "/blog/backend/framework/"
              },
              {
                "text": "方案",
                "link": "/blog/backend/solution/"
              },
              {
                "text": "工具",
                "link": "/blog/backend/tool/"
              }
            ]
          },
          {
            "text": "移动端",
            "items": [{
              "text": "微信小程序",
              "link": "/blog/mobile-end/wechat-tiny-app/"
            },
              {
                "text": "支付宝小程序",
                "link": "/blog/mobile-end/alipay-tiny-app/"
              }
            ]
          },
          {
            "text": "架构",
            "items": [{
              "text": "设计模式",
              "link": "/blog/architecture/design-patterns/"
            }]
          },
          {
            "text": "运维",
            "items": [{
              "text": "服务器",
              "link": "/blog/operation-and-maintenance/webserver/"
            },
              {
                "text": "操作系统",
                "link": "/blog/operation-and-maintenance/system/"
              }
            ]
          }
        ]
      },
      {
        "text": "三味书屋",
        "link": "/archive/"
      },
      /*{
        "text": "职场历险",
        "link": "/work/"
      },*/
      /*{
        "text": "摘抄",
        "link": "/excerpt/"
      }*/
    ],
    "sidebar": {
      "/daily/": [
        "",
        "2019/001",
        "2019/002",
        "2019/003",
        "2019/004",
        "2019/005",
        "2019/006",
        "2019/007",
        "2019/008",
        "2019/009",
        "2019/010",
        "2019/011",
        "2019/012",
        "2019/013",
        "2019/014",
        "2019/015",
        "2019/016",
        "2019/017",
        "2019/018",
        "2019/019",
        "2019/020",
        "2019/021",
        "2019/022",
        "2019/023",
        "2019/024",
        "2019/025",
        "2019/026",
        "2019/027",
        "2019/028",
        "2019/029",
        "2019/030",
        "2019/031",
        "2019/032",
      ],
      "/weekly/": [
        "",
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
      ],
      "/blog/front-end/": [{
        "title": "HTML",
        "collapsable": true,
        "children": []
      },
        {
          "title": "CSS",
          "collapsable": true,
          "children": []
        },
        {
          "title": "JavaScript",
          "collapsable": true,
          "children": []
        },
        {
          "title": "工具",
          "collapsable": true,
          "children": []
        }
      ],
      "/blog/backend/": [{
        "title": "PHP",
        "collapsable": true,
        "children": []
      },
        {
          "title": "数据库",
          "collapsable": true,
          "children": [
            "/blog/backend/database/mysql-right-way"
          ]
        },
        {
          "title": "框架",
          "collapsable": true,
          "children": [
            "/blog/backend/framework/Use-laravel-for-WeChat-development",
            "/blog/backend/framework/ThinkPhp",
            "/blog/backend/framework/laravel",
          ]
        },
        {
          "title": "方案",
          "collapsable": true,
          "children": []
        },
        {
          "title": "工具",
          "collapsable": true,
          "children": [
            "/blog/backend/tool/phpstorm-right-way"
          ]
        }
      ],
      "/blog/mobile-end/": [{
        "title": "微信小程序",
        "collapsable": true,
        "children": []
      },
        {
          "title": "支付宝小程序",
          "collapsable": true,
          "children": []
        }
      ],
      "/blog/architecture/": [{
        "title": "设计模式",
        "collapsable": true,
        "children": []
      }],
      "/blog/operation-and-maintenance/": [{
        "title": "服务器",
        "collapsable": true,
        "children": []
      },
        {
          "title": "操作系统",
          "collapsable": true,
          "children": [
            "/blog/operation-and-maintenance/system/linux-shell-error",
            "/blog/operation-and-maintenance/system/linux-replace-apt-source",
            "/blog/operation-and-maintenance/system/linux-sudo-error",
          ]
        }
      ],
      "/archive/": [
        "",
       // "browser-bookmarks",
        // "learn-go",
        //"tools",
        //"awesome-list",
        //"reddit",
        //"inspiration"
      ],
      "/work/": [
        ""
      ],
      "/": [
        ""
      ]
    },
    "sidebarDepth": 2,
    "lastUpdated": "最后更新于",
    "search": true,
    "searchMaxSuggestions": 10
  }
}