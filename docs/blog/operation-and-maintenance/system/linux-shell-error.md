### Linux：shell 脚本执行错误

Win 下编写的 shell 脚本复制到 Linux 下时，提示`$'\r':command not found`执行错误。

存现这种错误是因为编写的 shell 脚本是在win下编写的，每行结尾是 `\r\n`的，而 Unix 结果行是`\n`。

所以在 Linux下运行脚本会认为`\r`是一个字符，所以运行错误，需要把文件中的 `\r\n`转换下：

```
dos2unix  脚本名
```

Okay~
