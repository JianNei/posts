# ubuntu 更换阿里源

## 前言

网上应该可以找到很多关于 ubuntu 源的设置方法，但是如果不搞清楚就随便设置的话，不仅不能起到应有的效果，还会由于一些问题导致 apt 不可用。

最正确的更换源的方法应该如系统提示的：

```
## a.) add 'apt_preserve_sources_list: true' to /etc/cloud/cloud.cfg
## or do the same in user-data
## b.) add sources in /etc/apt/sources.list.d
## c.) make changes to template file /etc/cloud/templates/sources.list.tmpl
```

这种方法有点没弄明白在`/etc/apt/sources.list.d`应该添加的是什么内容，如果是源文件的话，最后更改的模板又是什么作用？也没有去尝试，有时间会解决一下。

## 步骤

以下提供设置阿里源的方法，其他源也可以如法炮制。

### 1. 查看系统的 codename

首先查看自己的 ubuntu 系统的 codename，这一步很重要，直接导致你更新的源是否对你的系统起效果，查看方法：

```
lsb_release -a
```

如，我的系统显示：

```
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 14.04.2 LTS
Release:	14.04
Codename:	trusty
```

显示了一些 ubuntu 的版本信息，需要得到的是 Codename，比如，我这里是 trusty。

### 2. 确认阿里源是否支持

登陆以下网页：http://mirrors.aliyun.com/ubuntu/dists/

该网页显示了阿里云支持的 ubuntu 系统下各个 Codename 版本，确保自己的 Codename 在该网页中存在（一般都会有的）

### 3. 备份系统源

```
cd /etc/apt
sudo mv sources.list sources.list_bak
```

### 4. 添加新的源文件：

```
sudo vi sources.list
```

并添加以下内容：注意，每一行的 trusty 应该用第一步查看得到的 Codename 来代替

```
deb http://mirrors.aliyun.com/ubuntu/ trusty main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ trusty-backports main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ trusty-proposed main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ trusty-security main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ trusty-updates main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ trusty main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ trusty-backports main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ trusty-proposed main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ trusty-security main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ trusty-updates main multiverse restricted universe
```

保存并且 `sudo apt-get update`，更新成功。

### 5. 配置脚本

```
Codename=$( (lsb_release -a)|awk '{print $2}'|tail -n 1 )
echo "\
deb http://mirrors.aliyun.com/ubuntu/ $Codename main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ $Codename-backports main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ $Codename-proposed main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ $Codename-security main multiverse restricted universe
deb http://mirrors.aliyun.com/ubuntu/ $Codename-updates main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ $Codename main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ $Codename-backports main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ $Codename-proposed main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ $Codename-security main multiverse restricted universe
deb-src http://mirrors.aliyun.com/ubuntu/ $Codename-updates main multiverse restricted universe ">sources.list
apt-get update
```

sudo 运行该脚本即可（注意运行之前最好备份之前的 sources.list）

## 参考资源

* [ubuntu更换阿里源](https://www.cnblogs.com/lyon2014/p/4715379.html)