## sudo 出现 unable to resolve host 解决方法

ubuntu 环境，假设这台机器名字叫 abc（机器的 hostname), 每次执行 sudo 就出现这个警告讯息：
```
sudo: unable to resolve host abc
```

虽然 sudo 还是可以正常执行，但是警告讯息每次都出来，而这只是机器在反解上的问题，
所以就直接从 /etc/hosts 设定，让 abc(hostname) 可以解回 127.0.0.1 的 IP 即可。
/etc/hosts 原始内容
```
127.0.0.1       localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

 在 127.0.0.1 localhost 后面加上主机名称 (hostname) 即可，/etc/hosts 内容修改成如下：
 
 ```
127.0.0.1       localhost abc  #要保证这个名字与 /etc/hostname 中的主机名一致才有效
# 或改成下面这两行 
#127.0.0.1       localhost 
#127.0.0.1       abc
```
这样设完后，使用 sudo 就不会再有那个提示信息了。