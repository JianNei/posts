# PhpStorm 的正确使用姿势

> 工欲利其事，必先利其器。

在日常的 PHP 项目开发中，最常使用的工具当属 PhpStorm 了，你真的已经完全了解她的使用了吗？👀

这里，将带你进一步认识你开发过程的“亲密伙伴”——PhpStorm。

从**安装激活**、**界面美化**、**配合工具使用**，最后再到频繁使用到的**代码 / 文件编辑快捷键**，来进一步认识 Phpstorm 从而来提高开发效率，已经开发体验。💹

## 一、下载以及激活

### 1. 下载

官网下载地址：[https://www.jetbrains.com/zh/phpstorm/specials/phpstorm/phpstorm.html?utm_source=baidu&utm_medium=cpc&utm_campaign=cn-bai-br-phpstorm-ph-pc&utm_content=phpstorm-prue&utm_term=phpstorm&gclid=CLnAsJzD9tsCFQgNvAodDCYDsw&gclsrc=ds&dclid=CMinupzD9tsCFQeclgodFngDCQ](https://www.jetbrains.com/zh/phpstorm/specials/phpstorm/phpstorm.html?utm_source=baidu&utm_medium=cpc&utm_campaign=cn-bai-br-phpstorm-ph-pc&utm_content=phpstorm-prue&utm_term=phpstorm&gclid=CLnAsJzD9tsCFQgNvAodDCYDsw&gclsrc=ds&dclid=CMinupzD9tsCFQeclgodFngDCQ)

### 2. 激活方法

激活步骤参考：[https://www.imsxm.com/jetbrains-license-server.html](https://www.imsxm.com/jetbrains-license-server.html)

**使用方法：** 激活时选择 License server 填入 [http://idea.imsxm.com](http://idea.imsxm.com/) 点击 Active 即可

* https://www.imsxm.com/2018/07/idea-2018-1-5-crack-patcher.html
* https://zhile.io/2018/08/17/jetbrains-license-server-crack.html

## 二、界面简化

**PhpStorm** 默认的界面布局稍显拥挤，尤其是在屏幕尺寸较小的时候，实际可用的工作区域就更少得可怜了。

个人倾向于，在有限的屏幕尺寸下，PhpStorm **工作区域更大**，**界面更加简洁**，**受干扰的元素更少**，能把关注点更多地放在编写或排查代码上。

### 1. 界面美化

#### 1.1 安装 **Material Theme UI** 插件

* `File  -> Setting -> Plugins -> Browse repositories... ->  Material Theme UI -> Install`
* `Ctrl + Alt + S -> Plugins -> Browse repositories... ->  Material Theme UI -> Install`
* `Ctrl + Shift + A -> Plugins -> Browse repositories... ->  Material Theme UI -> Install`
  
#### 1.2 选择主题

 **Material Theme UI** 插件安装成功以后，会多出 9 套主题可供选择。

### 2. 界面布局调整

#### 2.1 隐藏一些操作栏位

  * 隐藏快捷操作栏（Toolbar）：`View -> Toolbar` 去掉前面的勾选
  * 隐藏工具按钮（Tool buttons）：`View -> Tool buttons` 去掉前面的勾选
  * 隐藏状态栏 （Status bar）：`View -> Status bar` 去掉前面的勾选
  * 隐藏导航栏（Navigation bar）：`View -> Navigation bar` 去掉前面的勾选
  
#### 2.2 编辑区（editor）调整

快捷键 `Ctrl + Shift + A` （搜索配置项）：

* `tab placement`： 编辑区（editor）显示在上侧的标签
* `breadcrumbs` ：编辑区（editor） 显示在上 / 下位置的面包屑导航
* `show line number`： 编辑区（editor）显示在左侧的行数
* `code folding` ：编辑区（editor）显示在左侧的代码折叠（+/-）
* `show method separators`： 编辑区（editor） 中间区域，方法名之间的横线
* `show right margin` ：编辑区（editor） 右侧区域
* `show parameter name hints`：`Editor -> General -> Appearance -> show parameter name hints` 去掉函数参数提示（针对 PhpStorm 2017 以上版本）
  
## 三、配合其他工具使用

### 1. 配合 git 进行版本控制

#### 1.1 配置 git

* `File  -> Setting -> Version Control -> Git -> Path to Git executable`：设置 git.exe 的安装路径
* `Ctrl + Alt + S  -> Setting -> Version Control -> Git -> Path to Git executable`：设置 git.exe 的安装路径
  
#### 1.2 更新代码

* `VCS（Version Control Systems）-> Update Project`
* `Ctrl + T`

#### 1.3 查看本地修改

* `Alt + 9 -> Local Changes`
* `Ctrl + Shift + A -> Version Control -> Local Changes`

#### 1.4 提交代码

* `VCS（Version Control Systems）-> Commit`
* `Ctrl + K`
* 提交代码之前的额外操作
  * 代码格式化
  * 优化代码中的引入
  * 配合代码分析工具，进行代码分析，是否符合 PSR-0 或 PSR-4 风格
  * 检查待办 TODO
* 提交代码之后的额外操作
  * 上传文件到 FTP 服务器

#### 1.5 查看提交日志

* `Alt + 9 -> Log`
* `Ctrl + Shift + A -> Version Control -> Log`

#### 1.6 查看文件修改记录

* 编辑区左侧单击鼠标右键 -> Annotate
  * 单击鼠标左侧查看提交记录
  * 单击鼠标右侧
    * `Close annotations` ：关闭注释
    * `Show Diff` ：对比上一版本差异
    * `View`：配置注释显示的内容
    * `Copy revison number`：复制当前修订版本的序列号
    * `Annotate revison`：新标签页打开当前修订版本的文件
    * `Annotate Previous revison`：新标签页打开上一个修订版本的文件
    * `Select in git log` : 在提交日志中查询

##### 1.7 扩展 terminal

**（1） 配置**

* `Ctrl + Alt + S -> Tool -> Terminal -> Application settings -> Shell path` ：设置 git bin 的安装路径，如，*"D:\Program Files\Git\bin\sh.exe" --login -i*
* `Ctrl + Shift + A  -> Shell path ->  Shell path` ：设置 git bin 的安装路径，如，*"D:\Program Files\Git\bin\sh.exe" --login -i*
* 修改字体 ：
  * `Ctrl + Alt + S -> Editor -> Color Scheme -> Console Font`
  * `Ctrl + Shift + A  ->  Console Font`

**（2） 使用**

* `Tool buttons ->Terminal`
* `Alt + F12`
* `Ctrl + E -> Terminal`
* `Ctrl + Shift + A  ->  Terminal`

### 2. 配合 XDebug 进行断点调试

#### 2.1 PHP 集成环境中 XDebug 的安装

* 检查 php 安装目录是否有 XDebug 扩展
* php.ini 文件中配置开启 Xdebug（配置参考）

```
zend_extension = "E:\phpStudy\PHPTutorial\php\php-5.3.29-nts\ext\php_xdebug.dll"
xdebug.profiler_append = 0
xdebug.profiler_enable = 1
xdebug.profiler_enable_trigger = 0
xdebug.profiler_output_dir="E:\phpStudy\PHPTutorial\tmp\xdebug"
xdebug.trace_output_dir="E:\phpStudy\PHPTutorial\tmp\xdebug"
xdebug.profiler_output_name = "cache.out.%t-%s"
xdebug.remote_enable = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_host = "127.0.0.1"
xdebug.idekey="PHPSTORM"
```

主要是 zend_extension 和 xdebug.idekey 配置项的值要求准确。

* 通过 phpinfo() 输出信息检查是否有准确开启 Xdebug 扩展 

#### 2.2 谷歌浏览器中 XDebug 扩展的安装

（1）配合 PhpStorm 进行断点调试

（2）常见断点调试问题

* PhpStorm 中无法打断点
  * `View -> Active Edtior -> Show gutter icons`
  * `Ctrl + Shift + A ->  gutter icons`
* PhpStorm 中打了断点后没有生效
  * `Run -> Start Listening For PHP Debug Connetions`
  * `Run -> Break at first line in PHP scripts`

### 3. 配合 PHPUnit 进行单元测试（TODO）

### 4. 配合 PHP CS Fixer 进行代码分析

### 5. 自带的 Datebase 使用

#### 5.1 配置

* `View -> Tool window -> Database`
* `Tool Buttons ->  Database`
* `Ctrl + Shift + A -> Database`

#### 5.2 连本地数据库进行增删改查

#### 5.3 连远端数据库进行增删改查

#### 5.4 SQL 文件中的快捷操作

* 选中 sql 语句后鼠标右键单击选择 `Execute`
* 选中 sql 语句 按 `Ctrl + Enter` 组合快捷键

#### 5.5 PHP 文件中的快捷操作

* 写 SQL 语句时的字段补全提示

## 四、技巧来了

### 1. 文件的查找与切换

#### 1.1 设置文件编码

* `Ctrl + Alt + S -> Edtior -> File encodings` 
* `Ctrl + Shift + A -> File encodings`

#### 1.2 文件的快速创建

* 当前文件所在目录创建文件：`Alt + Home -> Alt + Insert`
* 选择其他目录创建文件：`Alt + Home -> 使用上下左右箭头切换目录 -> Alt + Insert`

#### 1.3 文件的查找

* `Ctrl + Shift + N` ：按照文件名查找文件
* `Ctrl + N` ：按照类名查找文件
* `Ctrl + Alt + Shift  + N` ：按照定义的方法名 / 变量名查找文件

#### 1.4 文件的切换

* `Ctrl + E` ：最近操作（查看 / 编辑）的文件，可以直接输入文件全称或文件名称缩写匹配
* `Ctrl + Shift + E` ：最近编辑过的文件，可以直接输入文件全称或文件名称缩写匹配
* `Ctrl + Tab` ：在最近操作的文件清单中进行快速切换
* `Alt + 右方向箭头`：切换到下一个标签页
* `Alt + 左方向箭头`：切换到上一个标签
* `Ctrl + F4` : 关闭当前文件

#### 1.5 打开文件所在文件夹

* `Ctrl + Alt + F12`：打开文件所在文件夹

#### 1.6 文件中的高亮语法错误

* `F2` ：下一个语法错误
* `Shift + F2`：上一个语法错误

### 2. 代码的定位与编辑

#### 2.1 代码定位

* 查找 / 替换
  * `Ctrl + F` : 当前文件中查找
  * `Ctrl + Shift + F` ：项目文件中查找（可能会与 window 快捷键冲突）
  * `Ctrl + R` ：当前文件替换
  * `Ctrl + Shift + R`：项目文件中替换
  * `Ctrl + G` ：跳转到指定行和列
* 函数定位
  * `Ctrl + F12` ：输入函数名称来匹配，允许输入函数名称简写来模糊匹配，允许查找从父类继承到的成员函数
  * `Alt + 7` ：输入函数名称来匹配，允许输入函数名称简写来模糊匹配，允许查找从父类继承到的成员函数
  * `Alt + 向上箭头`：跳转到上一个函数
  * `Alt + 向下箭头`：跳转到下一个函数
* 变量定位
  * `Ctrl + b`：查看变量定义，会跳转到变量定义的位置
  * `Ctrl + Shift + i -> F4`：查看变量定义，不会跳转
* 查看上一步 / 下一步编辑位置
  * `Ctrl + Alt + 左向箭头`：跳转到上一步编辑位置
  * `Ctrl + Shift + Backspace`：
* 层次关系
  * `Ctrl + H` ：类的层次关系，可以导出到文件
  * `Ctrl + Shift + H`：方法的层次关系
  * `Ctrl + Alt + H`：方法调用的层次关系
* 括号匹配
  * `Ctrl + [`
  * `Ctrl + ]`

#### 2.2 代码编辑

* 复制当前行
  * `Ctrl + D`
* 合并成一行
  * `Ctrl + Shift + J`
* 换行
  * `Ctrl + Enter` : 向下添加一行
  * `Ctrl + Shift + Enter`：向上添加一行
* 多点编辑
  * `Alt + J`：选中相同区域进行编辑
* 区域选中编辑
  * `Ctrl + W`：逐步扩大选中区域
  * `Ctrl + Shift + W` 逐步收缩选中区域
  * `Ctrl + Shift + [` ：选中光标右侧到匹配的花括号之间代码
  * `Ctrl + Shift + ]` ：选中光标左侧到匹配的花括号之间的代码
* 重构
  * `Shift + F6`（可以快速连续按两次） ：函数体内快速修改同一变量名称
  * `Ctrl + Shift + U` ：变量大小写转换

#### 2.3 代码生成

* 代码生成
  * `Alt + Insert：override methods/implement methods/getters /setters/constructor`
  * `Alt + Enter` ：生成注释 （`Setting -> Editor -> Intentions -> Generate PHPDoc for function`）
* 代码补全
  * `Ctrl + Shift + Enter` ：代码补全，比如 if(true)  ，try
* 代码包围
  * `Ctrl + Alt + T`：选中代码后使用语句结构包围选中区域代码

#### 2.4 代码格式化

* `Ctrl + Alt + L`：格式化整个文件代码
* `Ctrl + Alt + Shift + L`：弹出对话框可以选择格式化整个文件或选中文本

### 3. 其他辅助快捷操作

#### 3.1 工具栏切换

`View -> Tool window` 可以查看到下组快捷键的说明：
* `Alt + 1` ：项目目录结构
* `Alt + 2` ：收藏栏，书签（`ctrl + F11` 添加或移除书签；`Shift + F11` 单独显示书签）和调试断点集合 
* `Alt + 6` ：待办事项（TODO/todo，查看整个项目，查看最近文件，查看当前文件中的待办） 
* `Alt + 7` ：当前文件结构，成员函数、成员属性、继承的成员函数和成员属性等
* `Alt + 9` ：版本控制，查看 git 提交日志记录；查看本地工作区最近修改未提交 / 推送的文件
* `Alt + F12` ：Terminal

#### 3.2 Tab 上的快捷操作

* `Ctrl + F4` ：关闭 tab 
* 关闭其他 tab
* `Ctrl + Shift + C`：复制文件绝对路径
* `Ctrl + Alt + Shift +C` : 复制文件相对路径
* 当前文件进行水平 / 垂直分隔
* Local history
* git 操作

#### 3.3 File Template（配置不同类型文件的创建模板）

#### 3.4 Live Template（配置代码片段）

默认代码片段补全是 Tab 键：

* eco('echo' statement)
* fore(foreach(iterable_expr as $value) {...})
* forek(foreach(iterable_expr as $key => $value) {...})
* inc('include' statement)
* inco('include_once' statement)
* prif(private function)
* prisf(private static function)
* prof(protected function)
* prosf(protected static function)
* pubf(public function)
* pubsf(public static function)
* rqr('require' statement)
* rqro('require_once' statement)
* thr(throw new)

## 四、插件安装

* Laravel
* Material theme ui
* .env support

希望本文对你能有所帮助，Enjoy.❤