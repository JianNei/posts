# 使用 Laravel 进行微信公众号开发

记录使用 Laravel 开发微信公众号的学习过程。

## Laravel

### 创建项目

说明：当前 laravel 版本为 5.8

`composer create project laravel/laravel some --prefer-dist`

### 安装 Laravel-wechat

```
# Laravel < 5.8
composer require "overtrue/laravel-wechat:~4.0"
# Laravel >= 5.8
composer require "overtrue/laravel-wechat:~5.0"
```

### 配置

```
php artisan vendor:publish --provider="Overtrue\LaravelWeChat\ServiceProvider"
```

### 使用

1. 在中间件 `App\Http\Middleware\VerifyCsrfToken` 排除微信相关的路由

```
protected $except = [
    // ...
    'wechat',
];
```

2. 下面以接收普通消息为例写一个例子：

假设您的域名为 `overtrue.me` 那么请登录微信公众平台 “开发者中心” 修改 “URL（服务器配置）” 为： `http://overtrue.me/wechat`。

路由：

```
Route::any('/wechat', 'WeChatController@serve');
```

（后续考虑使用 DingoApi 接管微信路由）

注意：一定是 Route::any, 因为微信服务端认证的时候是 GET, 接收用户消息时是 POST ！

然后创建控制器 `WeChatController`：`php artisan makr:controller WeChatController`

```
<?php

namespace App\Http\Controllers;

use Log;

class WeChatController extends Controller
{

    /**
     * 处理微信的请求消息
     *
     * @return string
     */
    public function serve()
    {
        Log::info('request arrived.'); # 注意：Log 为 Laravel 组件，所以它记的日志去 Laravel 日志看，而不是 EasyWeChat 日志

        $app = app('wechat.official_account');
        $app->server->push(function($message){
            return "欢迎关注 overtrue！";
        });

        return $app->server->serve();
    }
}
```

上面例子里的 Log 是 Laravel 组件，所以它的日志不会写到 EasyWeChat 里的，建议把 wechat 的日志配置到 Laravel 同一个日志文件，便于调试。

### 部署

使用 deployer 本地部署需要将 `.env`文件复制到 shared 目录。（考虑脚本方式优化）

## 微信公众号

### 开启开发者模式

`开发 -> 基本配置 -> 服务器配置`
![Use-laravel-for-WeChat-development-2019-4-27-7-17-54.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/Use-laravel-for-WeChat-development-2019-4-27-7-17-54.png)

### ip 白名单

在使用服务端调用微信公众号接口时需要设置微信公众号平台 ip 白名单：`开发 -> 基本配置 -> 基本配置 -> IP 白名单`

```
Request access_token fail: {"errcode":40164,"errmsg":"invalid ip 39.107.92.208, not in whitelist hint: [4PgWqA09362994]"}
```

### 开发者工具

https://mp.weixin.qq.com/cgi-bin/frame?t=advanced/dev_tools_frame&nav=10049&token=1484282293&lang=zh_CN

## 准确开发姿势

* 服务器配置测试号
* 网页授权获取用户基本信息

## 问题

* rediret_uri 参数错误
![Use-laravel-for-WeChat-development-2019-5-1-16-47-17.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/Use-laravel-for-WeChat-development-2019-5-1-16-47-17.png) 
* 解决：不需要加 Http 协议
  ![Use-laravel-for-WeChat-development-2019-5-1-17-9-11.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/Use-laravel-for-WeChat-development-2019-5-1-17-9-11.png)
* 参考：
  * https://blog.csdn.net/tengdazhang770960436/article/details/51286808
  * https://blog.csdn.net/qq_37936542/article/details/78981369
* 如何放大微信 web 开发者工具中的内容
* 解决：使用微信开发者工具调试（可以切换屏幕尺寸）

## 参考资源

[laravel-wechat](https://github.com/overtrue/laravel-wechat)
[easywechat](https://www.easywechat.com/docs/4.1/official-account/tutorial)