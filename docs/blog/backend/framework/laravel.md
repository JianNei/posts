# Laravel 

* 5.8 版本

## 随笔

* laravel 中的路由参数与控制器中的参数名称无关，与参数定义位置有关

```
# dingo api 中定义得路由
$api->put('/repos/{owner}/{repo}/contents/{path}', 'GithubController@createContent');// 创建文件

# 控制器中的参数定义
public function createContent(Request $request, $repo, $owner, $path)
{
    $data =  [
        'message'   => $request->input('message'),
        'content'   => base64_encode($request->input('content')),
        'branch'    => $request->input('branch'),
        'committer' => $request->input('committer'),
        'author'    => $request->input('committer'),
    ];

    dd($owner);// 输出 Hello-ZhiShan

    return $this->githubService->createContent($owner, $repo, $path,$data);
}

# 请求测试路由
zhishan.test/api/repos/JianNei/Hello-ZhiShan/contents/hello

# 等价
public function createContent($repo, $owner, $path,Request $request)
{
    $data =  [
        'message'   => $request->input('message'),
        'content'   => base64_encode($request->input('content')),
        'branch'    => $request->input('branch'),
        'committer' => $request->input('committer'),
        'author'    => $request->input('committer'),
    ];

    dd($owner);

    return $this->githubService->createContent($owner, $repo, $path,$data);
}
```

* PUT 方法提交内容需要以 x-www-form-urlencode 格式提交，否则收不到内容；query params 格式也可以；form-data 格式不行
![laravel-2019-5-13-0-37-36.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/laravel-2019-5-13-0-37-36.png)
* Postman 中可以选择 raw 格式，配合 Json(application/json) 模拟前端传 json 格式数据
![laravel-2019-5-13-0-48-42.png](https://vuepress-pic-bed.oss-cn-beijing.aliyuncs.com/laravel-2019-5-13-0-48-42.png)

* 创建模型时生成数据表迁移会自动生成复数的数据表 `php artisan make:model VuePress -m`
* resource 目录路径 `resource_path()`