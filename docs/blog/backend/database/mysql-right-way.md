#  使用 MySQL 的准确姿势 

这里将沿着**准确性**以及**效率性**2 个角度来整理由小米团队发布的 [MySQL 启发式规则建议](https://github.com/XiaoMi/soar/blob/master/doc/heuristic.md)。

- **规范性**：不遵守该规则，可能符合 MySQL 语法，但不是一种好的实践方式
- **准确性**：不遵守该规则建议某种情况下**可能**会出现错误
- **效率性**：遵守该规则建议可以提高效率

## 数据表 Table

### 效率性

索引/水平分割/垂直分割

## 查询 Select

### 单表查询

SELECT FIELDS FROM TABLE WHERE 1=1 AND ... LIMIT ORDER BY  

### 多表查询

## 插入 Insert

## 更新 Update

## 删除 Delete